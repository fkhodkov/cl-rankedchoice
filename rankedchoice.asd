(defpackage :info.fkhodkov.rankedchoice-system (:use :cl))
(in-package :info.fkhodkov.rankedchoice-system)

(asdf:defsystem rankedchoice
  :name "rankedchoice"
  :author "Fedor Khodkov <fkhodkov@gmail.com>"
  :version "0.5.0"
  :maintainer "Fedor Khodkov <fkhodkov@gmail.com>"
  :licence "GPL 3"
  :description "Conducting elections using various ranked-choice systems"
  :long-description ""
  :depends-on (#:cl-utilities #:regex #:iterate)
  :components
  ((:module
    :src
    :components
    ((:file "packages")
     (:file "utils" :depends-on ("packages"))
     (:module
      :stv
      :depends-on ("packages" "utils")
      :components ((:file "utils")
                   (:file "stv" :depends-on ("utils"))
                   (:file "prfound-wigm" :depends-on ("stv"))
                   (:file "scottish-stv" :depends-on ("stv"))))
     (:file "read-blt-file" :depends-on ("packages" "utils"))
     (:file "write-blt-file" :depends-on ("packages" "utils"))
     (:file "schulze" :depends-on ("packages" "read-blt-file" "utils"))
     (:file "qpq" :depends-on ("packages" "utils"))
     (:file "positional" :depends-on ("packages" "utils"))
     (:file "borda" :depends-on ("packages" "positional" "utils"))
     (:file "mock-election" :depends-on ("packages"))
     (:file "client" :depends-on ("packages" "read-blt-file" "utils"))
     (:file "approval" :depends-on ("packages" "positional" "utils"))))))
