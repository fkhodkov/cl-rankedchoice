;;;; Utilities for reading ballots from .blt files
(in-package :info.fkhodkov.rankedchoice.read-blt-file)

;; Modified from OpenSTV source code
(defparameter +blank-line-matcher+
  (compile-str "^\\s*(?#.*)?$"))

(defparameter +candidates-and-seats-matcher+
  (compile-str "^\\s*(\\d+)\\s+(\\d+)\\s*(?#.*)?$"))

(defparameter +withdrawn-matcher+
  (compile-str "^\\s*(-\\d+(?\\s+-\\d+)*)\\s*(?#.*)?$"))

(defparameter +string-matcher+
  (compile-str "^\\s*\"?((?[^\"]|\\\\\")+)\"?\\s*(?#.*)?$"))

(declaim (inline char-digit))

(defun char-digit (c)
  (declare (optimize (speed 3)) (character c))
  (- (char-code c) #.(char-code #\0)))

(defconstant +empty-ballot+ 0)

(defconstant +end-of-all-ballots+ -1)

(defconstant +initial-ballots-size+ 100)

(declaim (ftype (function ((simple-array fixnum)) (function (*) (or ballot fixnum)))
                ballot-reader))

(defun ballot-reader (translation-table)
  (declare (optimize (speed 3)))
  (let* ((len (length translation-table))
         (seen (make-array len :element-type 'boolean :initial-element nil))
         (result (make-array len :initial-element nil))
         (reading-phase :start)
         (depth 0)
         (weight 0)
         (current-number 0))
    (declare (small-natural current-number weight depth))
    (flet
        ((parse-space ()
           (ecase reading-phase
             (:reading
              (if (zerop current-number)
                  (progn (assert (null (aref result depth)))
                         (setf reading-phase :end-of-ballot))
                  (progn
                    (assert (not (aref seen current-number)))
                    (setf (aref seen current-number) t)
                    (let ((cand (aref translation-table current-number)))
                      (unless (= cand -1) (push cand (aref result depth)))
                      (setf reading-phase :next-level)
                      (when (aref result depth) (incf depth))))))
             (:reading-weight
              (if (zerop current-number)
                  (setf reading-phase :end-of-ballot)
                  (setf reading-phase :next-level weight current-number)))
             ((:next-level :start :end-of-ballot))))
         (parse-equal ()
           (ecase reading-phase
             (:reading
              (assert (not (aref seen current-number)))
              (setf (aref seen current-number) t)
              (let ((cand (aref translation-table current-number)))
                (unless (= cand -1) (push cand (aref result depth)))
                (setf reading-phase :same-level)))))
         (parse-digit (digit)
           (ecase reading-phase
             ((:next-level :same-level)
              (setf reading-phase :reading current-number digit))
             ((:reading :reading-weight)
              (setf current-number (+ (* 10 current-number) digit)))
             (:start (setf reading-phase :reading-weight current-number digit)))))
      (declare (inline parse-space parse-equal parse-digit))
      (lambda (stream)
        (iter
          (initially
           (iter (for i index-of-vector seen)
                 (setf (aref seen i) nil (aref result i) nil
                       reading-phase :start weight 0 depth 0
                       current-number 0)))
          (for pos from 0) (declare (fixnum pos))
          (for char = (read-char stream))
          (ecase char
            (#\Newline (finish))
            (#\# (iter (until (char= (read-char stream) #\Newline)))
                 (finish))
            (#\space (parse-space))
            (#\= (parse-equal))
            ((#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9)
             (parse-digit (char-digit char))))
          (yield
            (ecase reading-phase
              (:reading
               (assert (zerop current-number))
               (make-instance 'ballot :weight weight :order (subseq result 0 depth)))
              (:end-of-ballot
               (if (zerop depth) +end-of-all-ballots+
                   (make-instance 'ballot :weight weight :order (subseq result 0 depth))))
              (:reading-weight
               (assert (zerop current-number))
               +end-of-all-ballots+)
              (:start +empty-ballot+))))))))

(declaim (inline get-substring string->numbers))

(defun get-substring (string signature)
  (destructuring-bind (start . end) signature
    (subseq string start end)))

(defun string->numbers (string &key (separator #\space) (preserve-order t))
  (declare (optimize (speed 3)))
  (mapcar
   #'(lambda (n) (parse-integer n))
   (split-string separator string preserve-order)))

(defun read-blt-file-line (stream)
  (declare (optimize (speed 3)))
  "Reads next non-comment-only line from .blt file and returns its non-comment part."
  (let ((line (read-line stream nil)))
    (declare (type (or string null) line))
    (cond ((null line) nil)
          ((scan-str +blank-line-matcher+ line) (read-blt-file-line stream))
          (t line))))

(defmacro with-matches (((matcher-p regs) matcher string) &body body)
  (once-only (matcher string)
    `(multiple-value-bind (,matcher-p -start -len ,regs)
         (scan-str ,matcher ,string)
       (declare (ignore -start -len) ((or (simple-array cons) null) ,regs))
       ,@body)))

(declaim (ftype (function (string) (values fixnum fixnum)) candidates-and-seats))

(defun candidates-and-seats (string)
  (with-matches ((matched-p regs) +candidates-and-seats-matcher+ string)
    (assert matched-p)
    (destructuring-bind (candidates seats)
        (map 'list (lambda (pair) (parse-integer (get-substring string pair)))
             (subseq regs 1 3))
      (assert (>= candidates seats))
      (values candidates seats))))

(declaim (ftype (function (string)) withdrawn-list))

(defun withdrawn-list (string)
  (map 'list #'abs (string->numbers string)))

(declaim (ftype (function (* (function (*) (values fixnum list)) array) array)
                read-ballots))

(defun read-ballots (stream reader ballots)
  (declare (optimize (speed 3)) (function reader))
  (with-extending-array (ballots :initial-size +initial-ballots-size+)
    (let ((ballot (funcall reader stream)))
      (until (eql ballot +end-of-all-ballots+))
      (unless (eql ballot +empty-ballot+)
        (vector-push ballot ballots)
        (yield (subseq ballots 0))))))

(declaim (ftype (function (string)) process-string))

(defun process-string (string)
  (declare (optimize (speed 3)))
  (with-matches ((matched-p regs) +string-matcher+ string)
    (assert matched-p)
    (get-substring string (aref regs 1))))

(declaim (ftype (function (t fixnum)) read-candidates))

(defun read-candidates (stream candidates)
  (iter (repeat candidates)
        (for candidate = (read-blt-file-line stream))
        (assert candidate)
        (collect (process-string candidate))))

(defun read-description (stream)
  (format nil "~{~a~^~%~}"
          (iter (for line = (read-blt-file-line stream))
                (while line) (collect (process-string line)))))

(declaim (ftype (function (string)) get-withdrawn))

(defun get-withdrawn (line)
  (with-matches ((matched-p regs) +withdrawn-matcher+ line)
    (and matched-p (get-substring line (aref regs 1)))))

(defun read-blt-file (name)
  "Reads information about election from .blt file."
  (declare (optimize (speed 3)))
  (with-open-file (stream name)
    (multiple-value-bind (candidates seats) (candidates-and-seats (read-blt-file-line stream))
      (let* ((ballot-or-withdrawn (read-blt-file-line stream))
             (withdrawn (let ((withdrawn (get-withdrawn ballot-or-withdrawn)))
                          (when withdrawn (withdrawn-list ballot-or-withdrawn)))))
        (multiple-value-bind (forward-table inverse-table)
            (running-candidates-table candidates withdrawn)
          (let ((reader (ballot-reader forward-table))
                (ballots
                  (make-array +initial-ballots-size+ :adjustable t :fill-pointer 0)))
            (declare (list withdrawn) (small-natural candidates seats)
                     (simple-string ballot-or-withdrawn))
            (unless withdrawn
              (vector-push
               (with-input-from-string (in (concatenate 'string ballot-or-withdrawn '(#\Newline)))
                 (funcall reader in))
               ballots))
            (assert (>= (- candidates (the small-natural (length withdrawn))) seats))
            (let ((ballots (read-ballots stream reader ballots)))
              (values ballots (read-candidates stream candidates) (read-description stream)
                      seats inverse-table withdrawn))))))))
