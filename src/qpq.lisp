;;;; QPQ, a quota-preferential STV-like election rule
(in-package :info.fkhodkov.rankedchoice.qpq)

(deftype candidate-status ()
  `(member :hopeful :excluded :elected))

(defun qpq-election (ballots candidates seats translation)
  (let ((results (run-qpq-election
                  (make-instance
                   'qpq-election
                   :%ballots ballots
                   :%candidates (length translation)
                   :seats seats))))
    (print-results results candidates translation)))

(defun print-results (results candidates translation)
  (with-slots (quotients-history quota-history elected actions active-history) results
    (iter (for round from 0)
          (for action = (aref actions round))
          (until (eql action :end))
          (format t "~%Round ~d~%" round)
          (for quota in-vector quota-history)
          (for active in-vector active-history)
          (destructuring-bind (action-name . candidate-number) action
            (format t "~a ~a~%" action-name
                    (nth (aref translation candidate-number) candidates)))
          (format t "~20a~10,5f~%~20a~10,5f~%Candidate votes:~%"
                  "Quota:" (* 1d0 quota) "Active ballots:" (* 1d0 active))
          (iter (for c in candidates)
                (for j from 0)
                (format t "~20a~10,4f~%" c (* 1d0 (aref quotients-history round j)))
                (finally (fresh-line))))
    (format t "~{~#[No winners~;Winner is ~a~;Winners are ~a and ~a~:;Winners are ~@{~#[~; and~] ~a~^,~}~]~:}.~%" (mapcar (lambda (c) (nth (aref translation c) candidates)) (sort elected #'<)))))

(defclass qpq-election ()
  ((ballots :accessor election-ballots)
   (active-ballots :initform 0 :accessor active)
   (elected-by-inactive-ballots :initform 0 :accessor elected-by-inactive)
   (invalid-ballots :initform 0 :accessor invalid)
   (hopefuls :accessor list-of-hopefuls)
   (excluded :accessor list-of-excluded :initform nil)
   (candidates :accessor candidates)
   (elected :accessor elected :initform 0)
   (seats :initarg :seats :accessor seats)
   (results :accessor results :accessor results)))

(defclass qpq-results ()
  ((quotients-history :accessor quotients-history)
   (quota-history :accessor quota-history)
   (actions :accessor actions)
   (active-history :accessor active-history)
   (elected :initform nil :accessor elected)))

(defmethod initialize-instance :after ((results qpq-results)
                                       &key candidates &allow-other-keys)
  (with-slots (quotients-history quota-history actions active-history) results
    (setf quotients-history (make-array (list (* candidates candidates) candidates))
          quota-history (make-array (* candidates candidates) :fill-pointer 0)
          active-history (make-array (* candidates candidates) :fill-pointer 0)
          actions (make-array (* candidates candidates) :fill-pointer 0))))

(defun valid-p (ballot)
  (plusp (length (order ballot))))

(defclass qpq-ballot ()
  ((ballot :initarg :ballot :accessor ballot)
   (depth :initform 0 :accessor depth)
   (elected :initform 0 :accessor elected)))

(defclass qpq-candidate ()
  ((status :initform :hopeful :accessor status)
   (ballots :initform nil :accessor candidate-ballots)))

(defun make-qpq-ballot (election ballot index)
  (when (valid-p ballot)
    (let ((blt (make-instance 'qpq-ballot :ballot ballot)))
      (setf (aref (election-ballots election) index) blt)
      (incf (active election) (weight ballot))
      (dispatch-ballot election ballot index))))

(defun dispatch-ballot (election ballot index)
  (let ((value (/ (weight ballot) (length (aref (order ballot) 0)))))
    (dolist (c (aref (order ballot) 0))
      (push (cons value index) (candidate-ballots (aref (candidates election) c))))))

(defun quotient (election candidate)
  (iter (for (value . index) in (candidate-ballots candidate))
        (sum value into vc)
        (sum (* value (elected (aref (election-ballots election) index))) into tc)
        (yield (/ vc (1+ tc)))))

(defun hopeful-p (candidate)
  (eql (status candidate) :hopeful))

(defun quota (election)
  (/ (active election) (+ 1 (seats election) (- (elected-by-inactive election)))))

(defmethod initialize-instance :after ((election qpq-election)
                                       &key %candidates %ballots &allow-other-keys)
  (with-slots (ballots active-ballots invalid-ballots candidates hopefuls results) election
    (setf ballots (make-array (length %ballots) :initial-element nil)
          candidates (make-array %candidates :element-type 'qpq-candidate)
          hopefuls (make-range %candidates :type :list))
    (iter (for i index-of-vector candidates)
          (setf (aref candidates i) (make-instance 'qpq-candidate)))
    (iter (for ballot in-vector %ballots with-index b)
          (if (valid-p ballot)
              (make-qpq-ballot election ballot b)
              (incf invalid-ballots (weight ballot))))
    (setf results (make-instance 'qpq-results :candidates %candidates))))

(defun run-qpq-election (election)
  (let ((quotients (make-array (length (candidates election)))))
    (iter (for round from 0)
          (when (= (elected election) (seats election))
            (vector-push (cons :exclude-remaining nil) (actions (results election)))
            (iter (for c in (list-of-hopefuls election))
                  (setf (status (aref (candidates election) c)) :excluded)
                  (push c (cdr (aref (actions (results election)) round))))
            (finish))
          (when (= (+ (elected election) (length (list-of-hopefuls election))) (seats election))
            (vector-push (cons :elect-remaining nil) (actions (results election)))
            (iter (for c in (list-of-hopefuls election))
                  (setf (status (aref (candidates election) c)) :elected)
                  (push c (elected (results election)))
                  (push c (cdr (aref (actions (results election)) round))))
            (finish))
          (for quota = (quota election))
          (let ((results (results election)))
            (vector-push quota (quota-history results))
            (vector-push (active election) (active-history results))
            (iter (for i index-of-vector quotients)
                  (setf (aref quotients i) (quotient election (aref (candidates election) i)))
                  (setf (aref (quotients-history results) round i) (aref quotients i)))
            (for candidate-numbers =
                 (sort (list-of-hopefuls election) #'> :key (lambda (c) (aref quotients c))))
            (setf (list-of-hopefuls election) candidate-numbers)
            (if (>= (aref quotients (first candidate-numbers)) quota)
                (let ((elected (first candidate-numbers)))
                  (vector-push (cons :elected elected) (actions results))
                  (push elected (elected results))
                  (elect election elected (aref quotients elected)))
                (let ((excluded (car (last candidate-numbers))))
                  (vector-push (cons :excluded excluded) (actions results))
                  (push excluded (list-of-excluded election))
                  (setf (status (aref (candidates election) excluded)) :excluded)
                  (restart-election election))))
          (finally
           (vector-push :end (actions (results election)))
           (return (results election))))))

(defun elect (election candidate-number quotient)
  (transfer election candidate-number quotient)
  (incf (elected election)))

(defun exclude (election candidate-number)
  (transfer election candidate-number))

(defun transfer (election candidate-number &optional quotient)
  (let ((candidate (aref (candidates election) candidate-number))
        (results (results election))
        (status (if quotient :elected :excluded)))
    (setf (list-of-hopefuls election) (delete candidate-number (list-of-hopefuls election))
          (status candidate) status)
    (iter (for (value . index) in (candidate-ballots candidate))
          (for ballot = (aref (election-ballots election) index))
          (when quotient (setf (elected ballot) (/ 1 quotient)))
          (iter (for level in-vector (order (ballot ballot)) from (depth ballot) with-index d)
                (let ((cs (remove-if-not (lambda (c) (hopeful-p (aref (candidates election) c)))
                                         level)))
                  (when cs
                    (if (= (depth ballot) d)
                        (dolist (c cs)
                          (setf (car (find index (candidate-ballots (aref (candidates election) c))
                                           :key #'cdr))
                                (/ (weight (ballot ballot)) (length cs))))
                        (dolist (c cs)
                          (push (cons (/ value (length cs)) index)
                                (candidate-ballots (aref (candidates election) c)))))
                    (setf (depth ballot) d)
                    (leave)))
                (finally
                 (incf (elected-by-inactive election) (* value (elected ballot)))
                 (decf (active election) value))))))

(defun restart-election (election)
  (setf (active election) 0
        (elected-by-inactive election) 0)
  (iter (for unelected-index = (pop (elected (results election))))
        (while unelected-index)
        (push unelected-index (list-of-hopefuls election))
        (for unelected = (aref (candidates election) unelected-index))
        (setf (status unelected) :hopeful)
        (finally (setf (elected election) 0)))
  (iter (for candidate in-vector (candidates election))
        (setf (candidate-ballots candidate) nil))
  (iter (for ballot in-vector (election-ballots election) with-index b)
        (when ballot
          (setf (depth ballot) 0 (elected ballot) 0)
          (incf (active election) (weight (ballot ballot)))
          (dispatch-ballot election (ballot ballot) b)))
  (dolist (excluded (list-of-excluded election))
    (transfer election excluded)))

(add-election-method
 "QPQ" #'qpq-election
 :description
 "QPQ, a quota-preferential STV-like election rule
From Douglas R. Woodall’s article
(http://www.votingmatters.org.uk/ISSUE17/I17P1.PDF)

2.1. The count is divided into a sequence of stages.  At the start of each
     stage, each candidate is in one of three states, designated as elected,
     excluded and hopeful.
     .
     At the start of the first stage, every candidate is hopeful.  In each
     stage, either one hopeful candidate is reclassified as elected, or one
     hopeful candidate is reclassified as excluded.

2.2. At the start of each stage, each ballot is deemed to have elected some
     fractional number of candidates, in such a way that the sum of these
     fractional numbers over all ballots is equal to the number of candidates
     who are currently classed as elected.  At the start of the first stage,
     every ballot has elected 0 candidates.

2.3. At the start of each stage, the quotients of all the hopeful candidates are
     calculated, as follows.  The ballots contributing to a particular hopeful
     candidate c are those ballots on which c is the topmost hopeful candidate.
     The quotient assigned to c is defined to be q_c = v_c / (1 + t_c), where
     v_c is the number of ballots contributing to c, and t_c is the sum of all
     the fractional numbers of candidates that those ballots have so far
     elected.

2.4. A ballot is active if it includes the name of a hopeful candidate (and is a
     valid ballot), and inactive otherwise.  The quota is defined to be
     v_a / (1 + s - t_x), where v_a is the number of active ballots, s is the
     total number of seats to be filled, and t_x is the sum of the fractional
     numbers of candidates that are deemed to have been elected by all the
     inactive ballots.

2.5a. If c is the candidate with the highest quotient, and that quotient is
      greater than the quota, then c is declared elected.  In this case each of
      the v_c ballots contrib uting to c is now deemed to have elected 1 = q_c
      candidates in total (regardless of how man y candidates it had elected
      before c’s election); no change is made to the number of candidates
      elected by other ballots.  (Since these v_c ballots collectively had
      previously elected t_c candidates, and they have now elected
      v_c/q_c = 1 + t_c candidates, the sum of the fractional numbers of
      candidates elected by all voters has increased by 1.)  If all s seats have
      now been filled, then the count ends; otherwise it proceeds to the next
      stage, from paragraph 2.3.

2.5b.  If no candidate has a quotient greater than the quota, then the candidate
       with the smallest quotient is declared excluded.  No change is made to
       the number of candidates elected by any ballot.  If all but s candidates
       are now excluded, then all remaining hopeful candidates are declared
       elected and the count ends; otherwise the count proceeds to the next
       stage, from paragraph 2.3.
       ...the count should be restarted from scratch after each exclusion")
