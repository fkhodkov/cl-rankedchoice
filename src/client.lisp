;;;; Command-line client code
(in-package :info.fkhodkov.rankedchoice.client)

(defun run-blt-client (args)
  (assert (= (length args) 2))
  (multiple-value-bind (ballots candidates description seats translation withdrawn)
      (read-blt-file (second args))
    (let ((method (get-election-method (first args))))
      (assert method)
      (with-slots ((method-name name) fn) method
        (let ((ncandidates (length candidates))
              (nwithdrawn (length withdrawn)))
          (format t "Election: ~a~&~%" description)
          (format t "~d candidates running for ~d seats~%" ncandidates seats)
          (format t "~[No candidates have~;Candidate ~{~a~} has~:;\
Candidates ~{~a~#[~;, and ~;, ~]~} have~] withdrawn~%~%"
                  nwithdrawn (mapcar (lambda (c) (elt candidates (1- c))) withdrawn))
          (format t "Using ~a method~%~%" method-name)
          (if (= seats (- ncandidates nwithdrawn))
              (let ((elected (do-array (c translation)
                               (collect (nth c candidates)))))
                (format
                 t "Number of seats ~d equal to number of candidates ~d minus
number of withdrawals ~d. So, all non-withdrawn candidates are elected:~%~{~a~%~}"
                 seats ncandidates nwithdrawn elected))
              (funcall fn ballots candidates seats translation)))))))

