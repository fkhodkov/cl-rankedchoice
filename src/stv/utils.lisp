;;;; Utilities for STV counting
(in-package :info.fkhodkov.rankedchoice.stv)

(deftype candidate-status ()
  `(member :hopeful :elected :defeated :pending))

(defclass stv-candidate ()
  ((ballots :type list :initform nil :accessor ballots)
   (name :type simple-string :initarg :name :accessor name)
   (status :type candidate-status :initform :hopeful :accessor status)
   (votes :type rational :initform 0 :accessor votes)))

(declaim (ftype (function (stv-candidate) rational) votes)
         (ftype (function (stv-candidate) list) ballots))

(deftype candidates ()
  `(simple-array stv-candidate))

(defclass stv-election ()
  ((arithmetic :accessor arithmetic :type list)
   (ballots :accessor ballots :initarg :ballots :type ballots)
   (candidates :accessor candidates :type candidates)
   (comparison :accessor comparison :initarg :comparison :type list)
   (depths :accessor depths :type (simple-array small-natural))
   (elected :accessor elected :initform 0 :type small-natural)
   (exhausted :accessor exhausted :initform 0 :type rational)
   (hopefuls :accessor hopefuls :type small-natural)
   (invalid :accessor invalid :initform 0 :type small-natural)
   (pending-list :accessor pending-list :initform nil :type small-natural)
   (quota :accessor quota :type rational)
   (residue :accessor residue :initform 0 :type rational)
   (counting-round :accessor counting-round :initform 0 :type small-natural)
   (seats :accessor seats :initarg :seats :type small-natural)
   (shares :accessor shares :type rational)
   (votes-history :accessor votes-history :type (simple-array (* *) rational))))

(declaim (ftype (function (stv-election) ballots) ballots)
         (ftype (function (stv-election) (simple-array rational)) votes-history)
         (ftype (function (stv-election) (simple-array small-natural)) depths)
         (ftype (function (stv-election) rational) quota)
         (ftype (function (stv-election) (simple-array rational)) shares)
         (ftype (function (stv-election) small-natural)
                seats counting-round elected hopefuls)
         (ftype (function (stv-election) list) pending-list))

(defmacro with-arithmetic ((arithmetic &key f* f/ ff) &body body)
  (once-only (arithmetic)
    `(flet (,@(when ff `((,ff (x) (funcall (get-op ,arithmetic :f) x))))
            ,@(when f* `((,f* (x y) (funcall (get-op ,arithmetic :*) x y))))
              ,@(when f/ `((,f/ (x y) (funcall (get-op ,arithmetic :/) x y)))))
       ,@body)))

(defmethod initialize-instance :after ((election stv-election)
                                       &key ballots candidates seats translation
                                         precision (quota-type :droop) whole-quota
                                         tie-break
                                         &allow-other-keys)
  (let ((votes (reduce #'+ ballots :key #'weight)))
    (with-slots (quota (election-candidates candidates) arithmetic depths hopefuls comparison
                       votes-history shares invalid)
        election
      (setf election-candidates (make-candidates-table candidates translation)
            arithmetic (make-arithmetic precision)
            depths (make-array (length ballots) :initial-element 0 :element-type 'fixnum)
            hopefuls (length translation)
            comparison (cons #'basic-comparison tie-break)
            votes-history (make-array (list hopefuls hopefuls)
                                      :element-type 'rational
                                      :initial-element 0))
      (multiple-value-bind (shares invalid) (assign-ballots election)
        (setf quota (calculate-quota
                     quota-type (- votes invalid) seats (if whole-quota 1 (expt 10 precision)))
              (shares election) shares
              (invalid election) invalid))
      (with-arithmetic (arithmetic :ff ff)
        (format t "Quota: ~a~%" (ff quota))))))

(defmacro do-candidates ((cand candidates &key index result ballots name status votes)
                         &body iter-body)
  (once-only (candidates)
    `(do-array-with-slots (,cand ,candidates
                                 ((ballots ,ballots :type list)
                                  (name ,name :type simple-string)
                                  (status ,status :type candidate-status)
                                  (votes ,votes :type rational))
                                 :index ,index :result ,result
                                 :element-type stv-candidate)
       ,@iter-body)))

(declaim (inline candref))

(defun candref (table)
  (declare (optimize (speed 3)) (candidates table))
  (lambda (n)
    (declare (fixnum n))
    (aref table n)))

(defun calculate-quota (quota-type voters seats &optional (10**precision 1))
  (ecase quota-type
    (:droop
     (/ (1+ (floor (* voters 10**precision) (1+ seats)))
        10**precision))
    (:hare
     (/ (1+ (floor (* voters 10**precision) seats))
        10**precision))))

(defun make-arithmetic (precision)
  (let ((fmt (format nil "~~d.~~~d,'0d" precision))
        (factor (expt 10 precision))
        (mul (funcall (fixed precision) #'*))
        (div (funcall (fixed precision) #'/)))
    (list :f (lambda (arg)
               (multiple-value-bind (int frac) (truncate arg)
                 (format nil fmt int (* factor frac))))
          :* (lambda (arg1 arg2) (funcall mul arg1 arg2))
          :/ (lambda (arg1 arg2) (funcall div arg1 arg2)))))

(declaim (inline get-op))

(defun get-op (arithmetic op)
  (declare (optimize (speed 3)))
  (the function (getf arithmetic op)))

(defvar *predicates* (list := #'= :< #'< :> #'>))

(defmacro predicate (pred) `(the function (getf *predicates* ,pred)))
