;;;; General STV counting functions
(in-package :info.fkhodkov.rankedchoice.stv)

(defun run-wigm-election (election)
  (declare (optimize (speed 3)))
  (iter (record-votes-history election)
        (print-candidates election)
        (let* ((npending (length (pending-list election)))
               (winners (+ (elected election) npending))
               (not-yet-losers (+ winners (hopefuls election))))
          (cond ((= (seats election) winners)
                 (leave (complete-count election :defeated)))
                ((= (seats election) not-yet-losers)
                 (leave (complete-count election :elected)))
                (t (elect-winners election)
                   (if (pending-list election) (transfer-surplus election)
                       (defeat-loser election)))))))

(defun record-votes-history (election)
  (declare (optimize (speed 3)))
  (do-candidates (c (candidates election) :index i :votes t)
    (setf (aref (votes-history election) (counting-round election) i) votes))
  (incf (counting-round election)))

(defun print-candidates (election)
  (declare (optimize (speed 3)))
  (with-arithmetic ((arithmetic election) :ff ff)
    (format t "Round ~d:~%~{~a~^, ~}~%" (counting-round election)
            (map 'list (lambda (c) (ff (votes c))) (candidates election)))
    (format t "Non-transferrable: ~a~%" (ff (exhausted election)))
    (format t "Residual (due to rounding errors): ~a~%" (ff (residue election)))))

(defun basic-comparison (election c1 c2 pred)
  (declare (optimize (speed 3)))
  (let ((cr (candref (candidates election))))
    (funcall (predicate pred) (votes (funcall cr c1)) (votes (funcall cr c2)))))

(defun tie-break-by-order (election c1 c2 pred)
  (declare (optimize (speed 3)))
  (funcall (predicate pred) c1 c2))

(declaim (inline tie-break-with-history))

(defun tie-break-with-history (election c1 c2 pred &key up)
  (declare (optimize (speed 3)))
  (let ((round (counting-round election))
        (hist (votes-history election)))
    (ecase pred
      (:= (iter (for r from 0 to round) (declare (fixnum r))
                (when (/= (aref hist r c1) (aref hist r c2))
                  (return nil))
                (yield t)))
      ((:< :>)
       (let ((predicate (the function (getf *predicates* pred))))
         (macrolet ((for-r (&rest driver)
                      `(iter (for r ,@driver) (declare (fixnum r))
                             (for v1 = (aref hist r c1))
                             (for v2 = (aref hist r c2))
                             (when (funcall predicate v1 v2) (return t))
                             (when (funcall predicate v2 v1) (return nil)))))
           (if up (for-r from round downto 0)
               (for-r from 0 to round))))))))

(defun tie-break-by-most-recent-votes (election c1 c2 pred)
  (declare (optimize (speed 3)))
  (tie-break-with-history election c1 c2 pred :up t))

(defun tie-break-by-earliest-votes (election c1 c2 pred)
  (declare (optimize (speed 3)))
  (tie-break-with-history election c1 c2 pred))

(defun random-tie-break (lst)
  (declare (optimize (speed 3)) (list lst))
  (nth (random (length lst)) lst))

(defun assign-ballots (election)
  (declare (optimize (speed 3)))
  (let* ((len (length (ballots election)))
         (shares (make-array len :initial-element 0 :element-type 'rational))
         (invalid 0))
    (declare (small-natural invalid))
    (do-ballots (blt (ballots election)
                     :order t :weight t :index b :result (values shares invalid))
      (with-arithmetic ((arithmetic election) :f/ f/)
        (if (zerop (length order))
            (incf invalid weight)
            (let* ((head (aref order 0))
                   (share (f/ 1 (length head))))
              (iter (with cr = (candref (candidates election)))
                    (for c in head)
                    (for candidate = (funcall cr c))
                    (push b (ballots candidate))
                    (incf (votes candidate) (* (weight blt) share)))
              (setf (aref shares b) share)))))))

(defun elect-tranfer-pending (election number)
  (declare (optimize (speed 3)))
  (let ((candidate (funcall (candref (candidates election)) number)))
    (format t "Action: Elect ~a, transfer pending~%" (name candidate))
    (setf (status candidate) :pending)
    (decf (hopefuls election))
    (push number (pending-list election))))

(defun elect-winners (election)
  (declare (optimize (speed 3)))
  (do-candidates (c (candidates election) :votes t :index i)
    (when (and (hopeful-p c) (>= votes (quota election)))
      (elect-tranfer-pending election i))))

(defun transfer-surplus (election)
  (declare (optimize (speed 3)))
  (let* ((quota (quota election))
         (nelected (find-next-transfer election))
         (elected-candidate (funcall (candref (candidates election)) nelected))
         (surplus (- (votes elected-candidate) (quota election))))
    (setf (pending-list election) (delete nelected (pending-list election)))
    (incf (elected election))
    (setf (status elected-candidate) :elected)
    (with-arithmetic ((arithmetic election) :f/ f/ :ff ff)
      (transfer election nelected (f/ surplus (votes elected-candidate)))
      (format t "Action: Transferring highest surplus form ~a (~a)~%"
              (name elected-candidate) (ff surplus)))
    (incf (residue election) (- (votes elected-candidate) quota))
    (setf (votes elected-candidate) quota)))

(defun find-next-transfer (election)
  (declare (optimize (speed 3)))
  (find-next-candidate election :winner))

(defun find-loser (election)
  (declare (optimize (speed 3)))
  (find-next-candidate election :loser))
 
(defun find-next-candidate (election direction)
  (declare (optimize (speed 3)))
  (let ((pred (if (eql direction :loser) :< :>))
        (initial-list (if (eql direction :winner) (pending-list election)
                          (stv-election-hopeful-numbers election))))
    (iter (for rule in (comparison election))
          (for compare = (lambda (c1 c2) (funcall rule election c1 c2 pred)))
          (for equal-p = (lambda (c1 c2) (funcall rule election c1 c2 :=)))
          (for possible initially initial-list then (interesting-set possible compare equal-p))
          (unless (cdr possible) (leave (car possible)))
          (yield (random-tie-break possible)))))

(defun interesting-set (lst compare equal-p)
  (declare (optimize (speed 3)) (list lst) (function compare equal-p))
  (let* ((sorted (sort (copy-list lst) compare))
         (max (car sorted))
         (last-interesting (position-if-not (lambda (c) (funcall equal-p c max)) sorted)))
    (subseq sorted 0 last-interesting)))

(defun defeat-loser (election)
  (declare (optimize (speed 3)))
  (let ((nloser (find-loser election)))
    (let* ((loser (funcall (candref (candidates election)) nloser))
           (votes (votes loser)))
      (format t "Action: Defeat: ~a~%" (name loser))
      (with-arithmetic ((arithmetic election) :ff ff)
        (format t "Action: Transferring from defeated: ~a (~a)~%" (name loser) (ff votes)))
      (setf (status loser) :defeated)
      (decf (hopefuls election))
      (transfer election nloser 1)
      (incf (residue election) (votes loser))
      (setf (votes loser) 0))))

(defun complete-count (election new-status-for-rest)
  (declare (optimize (speed 3)))
  (let ((remaining (mapcar (candref (candidates election))
                           (stv-election-hopeful-numbers election))))
    (format t "Action: ~a remaining: ~{~a~^, ~}~%"
            (if (eql new-status-for-rest :elected) "Elect" "Defeat")
            (mapcar #'name remaining))
    (dolist (candidate remaining)
      (setf (status candidate) new-status-for-rest))
    (format t "Action: Count Complete~%")
    (let ((cs (do-candidates (c (candidates election) :status t :name t)
                (when (member status '(:elected :pending))
                  (collect name)))))
      (declare (list cs))
      (format t "~{~#[No candidates are~;Candidate ~a is~;Candidates ~a and ~a are~:;Candidates~@{~#[~; and~] ~a~^,~} are~]~} elected.~%" cs))))

(defun next-candidates (election b)
  (declare (optimize (speed 3)))
  (let ((order (order (aref (ballots election) b))))
    (let ((len (length order))
          (cr (candref (candidates election))))
      (iter (for d from (aref (depths election) b) below len) (declare (fixnum d))
            (for cs = (remove-if-not #'hopeful-p (aref order d) :key cr))
            (when cs
              (let ((same-level (= d (aref (depths election) b))))
                (unless same-level
                  (setf (aref (depths election) b) d))
                (leave (values cs same-level))))))))

(defun transfer (election c ratio)
  (declare (optimize (speed 3)) (fixnum c))
  (with-arithmetic ((arithmetic election) :f/ f/ :f* f*)
    (let* ((cr (candref (candidates election)))
           (old (funcall cr c)))
      (dolist (b (ballots old))
        (let ((transferring (f* ratio (aref (shares election) b))))
          (multiple-value-bind (new-candidate-numbers same-level) (next-candidates election b)
            (if new-candidate-numbers
                (let ((new-share (f/ transferring (length new-candidate-numbers))))
                  (if same-level (incf (aref (shares election) b) new-share)
                      (setf (aref (shares election) b) new-share))
                  (iter (for c1 in new-candidate-numbers)
                        (for new = (funcall cr c1))
                        (unless same-level
                          (push b (ballots new)))
                        (let ((transferred (* (weight (aref (ballots election) b)) new-share)))
                          (incf (votes new) transferred)
                          (decf (votes old) transferred))))
                (let ((exhausted (* (weight (aref (ballots election) b)) transferring)))
                  (incf (exhausted election) exhausted)
                  (decf (votes old) exhausted)))))))))

(declaim (inline stv-election-hopeful-numbers))

(defun stv-election-hopeful-numbers (election)
  (declare (optimize (speed 3)))
  (do-candidates (c (candidates election) :index i)
    (when (hopeful-p c) (collect i))))

(declaim (inline hopeful-p))

(defun hopeful-p (candidate)
  (declare (optimize (speed 3)))
  (eql (status candidate) :hopeful))

(defun make-candidates-table (candidates table)
  (let ((result (make-array (length table) :element-type 'stv-candidate)))
    (do-array (c table :index i :result result)
      (setf (aref result i) (make-instance 'stv-candidate :name (nth c candidates))))))
