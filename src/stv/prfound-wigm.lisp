;;;; Proportional Representation Foundation's Reference WIGM Rule
(in-package :info.fkhodkov.rankedchoice.prfound-wigm)

(defun prfound-wigm-count (ballots candidates seats translation)
  (run-wigm-election
   (make-instance
    'stv-election
    :ballots ballots
    :candidates candidates
    :seats seats
    :translation translation
    :quota-type :droop
    :precision 4
    :tie-break (list #'tie-break-by-order))))

(add-election-method
 "Prfound-WIGM" #'prfound-wigm-count
 :description
 "Proportional Representation Foundation's Reference WIGM Rule
Weighted Inclusive Gregory Method, 4 decimal places, (optional batch defeat (not
impelemented yet)).

Multiple-seat elections shall be counted as follows.

A. Initialize Election
   1. Set the quota (votes required for election) to the total number of valid
      ballots, divided by one more than the number of seats to be filled, plus
      0.0001.
   2. Set each candidate who is not withdrawn to hopeful.
   3. Test count complete (D.3).
   4. Set each ballot’s weight to one, and assign it to its top-ranked hopeful
      candidate.
   5. Set the vote for each candidate to the total number of ballots assigned to
      that candidate.

B. Round
   1. Elect winners. Set each hopeful candidate whose vote is greater
      than or equal to the quota to pending (elected with
      surplus-transfer pending). Set the surplus of each pending
      candidate to that candidate’s vote minus the quota. Test count
      complete (D.3).
   2. Defeat sure losers (optional). Find the largest set of hopeful candidates
      that meets all of the following conditions.
      a. The number of hopeful candidates not in the set is greater than or
         equal to the number seats to be filled minus pending and elected
         candidates).
      b. For each candidate in the set, each hopeful candidate with the same
         vote or lower is also in the set.
      c. The sum of the votes of the candidates in the set plus the sum of all
         the current surpluses (B.1) is less than the lowest vote of the hopeful
         candidates not in the set.
      If the resulting set is not empty, defeat each candidate in the set and
      test count complete (D.3), transfer each ballot assigned to a defeated
      candidate (D.2), and continue at step B.1.
   3. Transfer high surplus. Select the pending candidate, if any, with the
      largest surplus (possibly zero), breaking ties per procedure D.1. For each
      ballot assigned to that candidate, set its new weight to the ballot’s
      current weight multiplied by the candidate’s surplus (B.1), then divided
      by the candidate’s total vote. Transfer the ballot (D.2). If a
      surplus (possibly zero) is transferred, continue at step B.1.
   4. Defeat low candidate. Defeat the hopeful candidate with the lowest vote,
      breaking ties per procedure D.1. Test count complete (D.3). Transfer each
      ballot assigned to the defeated candidate (D.2). Continue at step B.1.

C. Finish Count
   Set all pending candidates to elected. If all seats are filled, defeat all
   hopeful candidates; otherwise elect all hopeful candidates. Count is
   complete.

D. General Procedures
   1. Break ties. Ties arise in B.3 (choose candidate for surplus transfer) and
      in B.4 (choose candidate for defeat). In each case, choose the tied
      candidate who is earliest in a predetermined random tiebreaking order.
   2. Transfer ballots. Reassign each ballot to be transferred to its
      highest-ranking hopeful candidate and add the current weight of the ballot
      to the vote of that candidate. If the ballot ranks no such candidate, or
      has a weight of zero, it is exhausted and no longer participates in the
      count.
   3. Test count complete. If the number of elected plus pending candidates is
      equal to the number of seats to be filled, or the number of elected plus
      pending plus hopeful candidates is equal to or less than the number of
      seats to be filled, the count is complete; finish at step C.
   4. Arithmetic. Truncate, with no rounding, the result of each multiplication
      or division to four decimal places.

Notes

   This rule is designed as a minimalist WIGM rule. The only concession to
   complexity is the inclusion of an optional defeat-sure-losers step. The
   inclusion of this step may be justified by the fact that it can make a hand
   count faster by avoiding some transfers (in particular, avoiding surplus
   transfers that would have otherwise gone to candidates that will be
   immediately and inevitably defeated).

   Defeat of sure losers is optional in the sense that the rule may be adopted
   without including the sure-loser step. However, if adopted, this step must be
   mandatory, because the outcome with it may differ from the outcome without
   it, as it can alter the order of surplus distribution and thus the outcome of
   the election.

Copyright 2010 by Jonathan Lundell
Licensed under a Creative Commons Attribution-Share Alike 3.0 United States
License.")
