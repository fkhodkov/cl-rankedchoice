(defpackage :info.fkhodkov.rankedchoice.utils
  (:use :common-lisp :iterate)
  (:import-from :cl-utilities :once-only)
  (:export #:ballots #:ballot #:order #:weight
           #:make-range #:make-candidate #:candidate-name
           #:fixed #:pprint-table #:split-string #:format-quit
           #:do-array #:do-array-with-slots #:do-ballots
           #:with-extending-array #:small-natural
           #:election-method #:name #:fn #:add-election-method
           #:get-election-method #:yield #:running-candidates-table))

(defpackage :info.fkhodkov.rankedchoice.stv
  (:use :common-lisp :iterate :info.fkhodkov.rankedchoice.utils)
  (:import-from :cl-utilities :once-only)
  (:export #:run-wigm-election
           #:tie-break-by-most-recent-votes
           #:tie-break-by-order
           #:stv-election))

(defpackage :info.fkhodkov.rankedchoice.read-blt-file
  (:use :common-lisp :iterate :info.fkhodkov.rankedchoice.utils :regex)
  (:import-from :cl-utilities :split-sequence :once-only)
  (:export #:read-blt-file))

(defpackage :info.fkhodkov.rankedchoice.write-blt-file
  (:use :common-lisp :iterate :info.fkhodkov.rankedchoice.utils)
  (:export #:write-blt-file #:output-blt))

(defpackage :info.fkhodkov.rankedchoice.prfound-wigm
  (:use :common-lisp :info.fkhodkov.rankedchoice.utils
        :info.fkhodkov.rankedchoice.stv))

(defpackage :info.fkhodkov.rankedchoice.scottish-stv
  (:use :common-lisp :info.fkhodkov.rankedchoice.utils
        :info.fkhodkov.rankedchoice.stv))

(defpackage :info.fkhodkov.rankedchoice.schulze
  (:use :common-lisp :iterate
        :info.fkhodkov.rankedchoice.utils)
  (:import-from :cl-utilities :once-only))

(defpackage :info.fkhodkov.rankedchoice.positional
  (:use :common-lisp :iterate :info.fkhodkov.rankedchoice.utils)
  (:import-from :cl-utilities :once-only)
  (:export #:positional))

(defpackage :info.fkhodkov.rankedchoice.borda
  (:use :common-lisp :info.fkhodkov.rankedchoice.positional :info.fkhodkov.rankedchoice.utils))

(defpackage :info.fkhodkov.rankedchoice.approval-vote
  (:use :common-lisp :info.fkhodkov.rankedchoice.positional :info.fkhodkov.rankedchoice.utils))

(defpackage :info.fkhodkov.rankedchoice.qpq
  (:use :common-lisp :iterate :info.fkhodkov.rankedchoice.utils)
  (:import-from :cl-utilities :once-only)
  (:export #:run-qpq-election))

(defpackage :info.fkhodkov.rankedchoice.mock-election
  (:use :common-lisp :info.fkhodkov.rankedchoice.utils :iterate
        :info.fkhodkov.rankedchoice.write-blt-file)
  (:export #:mock-election))

(defpackage :info.fkhodkov.rankedchoice.client
  (:use :common-lisp :iterate
        :info.fkhodkov.rankedchoice.read-blt-file
        :info.fkhodkov.rankedchoice.utils)
  (:export :run-blt-client))
