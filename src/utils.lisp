;;;; Utilities for rankedchoice
(in-package :info.fkhodkov.rankedchoice.utils)

(setf iter::*always-declare-variables* t)

(defmacro yield (&body body)
  `(finally
    ,@(butlast body)
    (return ,@(last body))))

(deftype small-natural () `(integer 0 ,(floor most-positive-fixnum 100)))

(declaim (ftype (function (character simple-string &optional boolean)) split-string))

(defun split-string (separator str &optional (preserve-order t))
  (declare (optimize (speed 3)))
  (let ((len (length str)))
    (do ((start 0) (status t) (result nil) (i 1 (1+ i)))
        ((= i len)
         (let ((result (if status (cons (subseq str start) result) result)))
           (if preserve-order (nreverse result) result)))
      (if status
          (when (char= (aref str i) separator)
            (setf status nil result (cons (subseq str start i) result)))
          (when (char/= (aref str i) separator) (setf status t start i))))))

(deftype ballot-order ()
  `(simple-array list))

(defclass ballot ()
  ((order :type ballot-order :initarg :order :reader order)
   (weight :type fixnum :initarg :weight :reader weight)))

(deftype ballots () `(simple-array ballot))

(defstruct election-method
  (name "" :type string)
  (description nil :type (or null string))
  (fn #'identity :type (function)))

(declaim (ftype (function (ballot) fixnum) weight)
         (ftype (function (ballot) ballot-order) order))

(defparameter *methods* nil)

(defun add-election-method (name fn &key description)
  (push (make-election-method :name name :fn fn :description description)
        *methods*))

(defun get-election-method (name)
  (find name *methods* :key #'election-method-name :test #'string=))

(defun fixed (precision)
  (let ((factor (expt 10 precision)))
    (lambda (op)
      (lambda (&rest args)
        (/ (truncate (* (apply op args) factor)) factor)))))

(defun quit (&optional code)
  ;; From Rob Warnock: http://www.cliki.net/Portable%20Exit
  ;; This group from "clocc-port/ext.lisp"
  #+allegro (excl:exit code)
  #+clisp (#+lisp=cl ext:quit #-lisp=cl lisp:quit code)
  #+cmu (ext:quit code)
  #+cormanlisp (win32:exitprocess code)
  #+gcl (lisp:bye code)                     ; XXX Or is it LISP::QUIT?
  #+lispworks (lw:quit :status code)
  #+lucid (lcl:quit code)
  #+sbcl (sb-ext:exit :code code)
  ;; This group from Maxima
  #+kcl (lisp::bye)                         ; XXX Does this take an arg?
  #+scl (ext:quit code)                     ; XXX Pretty sure this *does*.
  #+(or openmcl mcl) (ccl::quit)
  #+abcl (cl-user::quit)
  #+ecl (si:quit)
  ;; This group from <hebi...@math.uni.wroc.pl>
  #+poplog (poplog::bye)                    ; XXX Does this take an arg?
  #-(or allegro clisp cmu cormanlisp gcl lispworks lucid sbcl
        kcl scl openmcl mcl abcl ecl)
  (error 'not-implemented :proc (list 'quit code)))

(defun format-quit (code format-string &rest rest)
  (apply #'format *error-output* format-string rest)
  (quit code))

(defun pprint-table (table)
  (let ((rows (array-dimension table 0))
        (cols (array-dimension table 1)))
    (let* ((width
            (let ((max 0))
              (dotimes (row rows)
                (dotimes (col cols)
                  (when (> (aref table row col) max)
                    (setf max (aref table row col)))))
              (1+ (ceiling (log max 10)))))
           (frmt (concatenate 'string "~" (write-to-string width) "d")))
      (with-output-to-string (r)
        (dotimes (row rows)
          (format
           r "~a~%"
           (with-output-to-string (c)
             (dotimes (col cols)
               (format c frmt (aref table row col))))))))))

(defmacro do-array ((elem array &key index result element-type (array-type :simple))
                    &body iter-body)
  (let ((type-declaration
         (when element-type
          `(,(ecase array-type
               (:simple 'simple-array)
               (:array 'array)
               (:vector 'vector)
               (:simple-vector 'simple-vector))
             ,element-type))))
    `(iter (for ,elem in-vector ,array ,@(when index `(with-index ,index)))
           ,(when type-declaration `(declare (type ,type-declaration ,array)))
           ,@iter-body
           ,(when result `(yield ,result)))))

(defun destructure-slot-spec (name spec)
  (cond ((null spec) nil)
        ((listp spec)
         (if (eql (first spec) :type)
             (values name (second spec))
             (destructuring-bind (synonym &key type) spec
               (cond ((null synonym) nil)
                     ((eql synonym t) (values name type))
                     (t (values `(,synonym ,name) type))))))
        ((eql spec t) name)
        (t `(,spec ,name))))

(defun collect-slot-specs (slots)
  (iter (for slot in slots)
        (if (listp slot)
            (let ((name (first slot)))
              (multiple-value-bind (spec type) (destructure-slot-spec name (rest slot))
                (when spec (collect spec into specs))
                (when type (collect `(,type ,name) into types))))
            (collect slot into specs))
        (yield (values specs types))))

(defmacro do-array-with-slots ((elem array slots &key index result
                                     (array-type :simple) (element-type t))
                               &body iter-body)
  (multiple-value-bind (specs types) (collect-slot-specs slots)
    `(do-array (,elem ,array :index ,index :result ,result
                      :array-type ,array-type :element-type ,element-type)
       (,@(if specs
              `(with-slots ,specs ,elem ,@(when types `((declare ,@types))))
              `(progn))
          ,@iter-body))))

(defmacro do-ballots ((blt ballots &key index result order weight) &body iter-body)
  (once-only (ballots)
    `(do-array-with-slots (,blt ,ballots ((order ,order :type ballot-order)
                                          (weight ,weight :type fixnum))
                                :index ,index :result ,result :array-type :simple
                                :element-type ballot)
       ,@iter-body)))

(defmacro with-extending-array ((array-var &key initial-size) &body iter-body)
  (let ((length-var (gensym "len"))
        (size-var (gensym "size")))
    `(iter
       (with (the (integer 0 #.(floor most-positive-fixnum 2)) ,size-var)
             = ,(or initial-size `(array-total-size ,array-var)))
       (for (the fixnum ,length-var) first (length ,array-var) then (1+ ,length-var))
       (when (= ,length-var ,size-var)
         (adjust-array ,array-var (setf ,size-var (* 2 ,size-var))))
       ,@iter-body)))

(declaim (inline make-range))

(defun make-range (n &key (type :array) (start 0))
  (declare (optimize (speed 3)) (small-natural n start))
  (ecase type
    (:array
     (let ((result (make-array n :element-type 'small-natural)))
       (iter (for i index-of-vector result)
             (for ai from start) (declare (fixnum i ai))
             (setf (aref result i) ai)
             (yield result))))
    (:list
     (let ((result nil))
       (iter (repeat n)
             (for i downfrom (1- (+ start n))) (declare (fixnum i))
             (push i result)
             (yield result))))))

(declaim (ftype (function (fixnum list) (values (simple-array fixnum) (simple-array fixnum)))
                running-candidates-table))

(defun running-candidates-table (number-of-candidates withdrawn-list)
  (let ((forward (make-array (1+ number-of-candidates) :initial-element -1
                             :element-type 'fixnum))
        (inverse (make-array (- number-of-candidates (length withdrawn-list))
                             :element-type 'fixnum)))
    (iter (for i from 1 to number-of-candidates)
          (generate j index-of-vector inverse)
          (unless (member i withdrawn-list)
            (setf (aref forward i) (next j))
            (setf (aref inverse j) (1- i))))
    (values forward inverse)))
