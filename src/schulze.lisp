;;;; Schulze method
(in-package :info.fkhodkov.rankedchoice.schulze)

(deftype candidates-number () `(integer 0 ,#.(isqrt most-positive-fixnum)))

(defun defeats-table (ballots candidates)
  "Calculates defeats table from `ballots'.  
`candidates' is a list of non-withdrawn candidates running it the
election.  Result is 2D-array where `(aref result i j)' is a number of
ballots where `ith' candidate is higher than `jth'"
  (declare (optimize (speed 3)) (candidates-number candidates))
  (let* ((result (make-array (list candidates candidates)
                             :initial-element 0 :element-type 'small-natural))
         (remaining (make-array candidates :element-type 'boolean :initial-element t)))
    (do-ballots (ballot ballots :order t :weight t :result result)
      (dotimes (c candidates) (setf (aref remaining c) t))
      (do-array (cs order :element-type list)
        (dolist (c cs) (setf (aref remaining c) nil))
        (dolist (c1 cs)
          (declare (fixnum c1))
          (do-array (r remaining :index c2)
            (when r (incf (aref result c1 c2) weight))))))))

(declaim (ftype (function ((simple-array small-natural (* *)) fixnum) list) smith-set))

(defun smith-set (beat-table len)
  "Calculates the Smith set.
Smith set is the smallest possible set of candidates such that:
for any candidate `x' in the set, and
any candidate `y' not it the set:
    (> (aref beat-table i j) (aref beat-table j i)) is true.
Adapded from Marcus Schulze's pseudocode:
http://groups.yahoo.com/group/election-methods-list/message/6493"
  (let* ((dummy (make-array (list len len) :initial-element nil :element-type 'boolean))
         (isschwartz (make-array len :initial-element t :element-type 'boolean)))
    (dotimes (i len)
      (dotimes (j len)
        (when (and (/= i j) (> (aref beat-table i j) (aref beat-table j i)))
          (setf (aref dummy i j) t))))
    (dotimes (i len)
      (dotimes (j len)
        (when (and (/= i j) (aref dummy j i))
          (dotimes (k len)
            (when (and (/= i k) (/= j k) (aref dummy i k))
              (setf (aref dummy j k) t))))))
    (dotimes (i len)
      (dotimes (j len)
        (when (and (/= i j) (aref dummy j i))
          (dotimes (k len)
            (when (and (/= i k) (/= j k) (aref dummy i k))
              (setf (aref dummy j k) t))))))
    (dotimes (i len)
      (dotimes (j len)
        (when (and (/= i j) (not (aref dummy i j)) (aref dummy j i))
          (setf (aref isschwartz i) nil))))
    (do-array (schwartz-p isschwartz :index c)
      (when schwartz-p (collect c)))))

(defun candidates-path-table (beat-table smith-set)
  "Calculate strongest path strength between candidates in the Smith set.
See ://en.wikipedia.org/wiki/Schulze_method#Implementation"
  (let* ((len (length smith-set))
         (paths (make-array (list len len) :element-type 'fixnum :initial-element 0)))
    (prog1 paths
      (iter (for c1 in-sequence smith-set with-index i)
            (iter (for c2 in-sequence smith-set with-index j)
                  (when (/= i j)
                    (setf (aref paths i j)
                          (let ((dij (aref beat-table c1 c2)))
                            (if (> dij (aref beat-table c2 c1)) dij 0))))))
      (iter (for i index-of-sequence smith-set)
            (iter (for j index-of-sequence smith-set)
                  (when (/= i j)
                    (dotimes (k len)
                      (when (and (/= i k) (/= j k))
                        (setf (aref paths j k)
                              (max (aref paths j k)
                                   (min (aref paths j i) (aref paths i k))))))))))))

(declaim (ftype (function (simple-array fixnum)) schulze))

(defun schulze (ballots number-of-candidates)
  "Produce full results of Schulze method with `number-of-candidates'
candidates using `ballots'.  Results include the winner, the defeats
table, the Smith set and strongest path table between candidates in
the Smith set."
  (let* ((beat-table (defeats-table ballots number-of-candidates))
         (smith-set (smith-set beat-table number-of-candidates)))
    (if (null (cdr smith-set))
        (values smith-set beat-table smith-set nil)
        (let ((path-table (candidates-path-table beat-table smith-set)))
          (values
           (iter (with xs = (list 0))
                 (for y from 1 below (length smith-set))
                 (for path-table-xy = (aref path-table (car xs) y))
                 (for path-table-yx = (aref path-table y (car xs)))
                 (cond ((= path-table-xy path-table-yx) (push y xs))
                       ((< path-table-xy path-table-yx) (setf xs (list y)))
                       (t))
                 (yield (mapcar (lambda (x) (nth x smith-set)) xs)))
           beat-table smith-set path-table)))))

(declaim (inline candref))

(defun candref (table)
  (declare (optimize (speed 3)) (type (simple-array fixnum) table))
  (lambda (n)
    (declare (fixnum n))
    (aref table n)))

(defun schulze-output (ballots candidates seats translation)
  "Generates Schulze method output"
  (declare (optimize (speed 3)) (list candidates) (type (simple-array fixnum) translation))
  (flet ((candref (c) (funcall (candref translation) c)))
    (multiple-value-bind (result beat-table smith-set path-table)
        (schulze ballots (length translation))
      (concatenate
       'string
       (format t "Ballot file contains ~a ballots and ~a candidates~%"
               (reduce #'+ ballots :key #'weight) (length candidates))
       (format t "Pairwise defeats table:~%~a~%" (pprint-table beat-table))
       (format t "Smith set: ~{~a~^, ~}~%"
               (mapcar (lambda (c) (nth (candref c) candidates)) smith-set))
       (if path-table
           (format t "~%Beatpaths matrix: ~%~a~%" (pprint-table path-table))
           "")
       (format t "Winner: ~{~a~^, ~}~%"
               (mapcar (lambda (c) (nth (candref c) candidates)) result))))))

(add-election-method
 "Schulze" #'schulze-output
 :description
 "From Wikipedia:

The Schulze method is an electoral system developed in 1997 by
Markus Schulze that selects a single winner using votes that express
preferences. The method can also be used to create a sorted list of
winners. The Schulze method is also known as Schwartz Sequential
dropping (SSD), cloneproof Schwartz sequential dropping (CSSD), the
beatpath method, beatpath winner, path voting, and path winner.")
