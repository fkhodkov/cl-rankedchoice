;;;; Save election information to a .blt file
(in-package :info.fkhodkov.rankedchoice.write-blt-file)

(defun output-blt (ballots candidates description seats &key withdrawn (stream *standard-output*))
  (declare (optimize (speed 3)) (list candidates) (stream stream)
           (type (or ballots (function () (or null ballot))) ballots))
  (let ((number-of-candidates (length candidates)))
    (format stream "~d ~d~%" number-of-candidates seats)
    (when withdrawn
      (format stream "~{-~d~^ ~}~%" withdrawn))
    (multiple-value-bind (forward inverse)
        (running-candidates-table number-of-candidates withdrawn)
      (declare (ignore forward))
      (flet ((print-ballot (ballot)
               (format stream "~d " (weight ballot))
               (iter (for level in-vector (order ballot))
                     (format stream "~{~a~^=~} "
                             (mapcar (lambda (c) (1+ (aref inverse c))) level))
                     (finally (format stream "0~%")))))
        (etypecase ballots
          (ballots (iter (for ballot in-vector ballots)
                         (print-ballot ballot)))
          (function (iter (for ballot = (funcall ballots))
                          (while ballot)
                          (print-ballot ballot))))
        (format stream "0~%")))
    (format stream "~{~s~%~}" candidates)
    (format stream "~s~&" description)))

(defun write-blt-file (file-name ballots candidates description seats &key withdrawn if-exists)
  (declare (optimize (speed 3)))
  (with-open-file (out file-name :direction :output :if-exists if-exists)
    (output-blt ballots candidates description seats :withdrawn withdrawn :stream out)))
