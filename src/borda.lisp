;;;; Borda count
(in-package :info.fkhodkov.rankedchoice.borda)

(defun borda (ballots number-of-candidates)
  (positional (lambda (i) (- number-of-candidates i))
              ballots number-of-candidates))

(defun borda-output (ballots candidates seats translation)
  (let ((tbl
         (map 'list (lambda (c) (cons (nth (aref translation (car c)) candidates) (cdr c)))
              (borda ballots (length translation)))))
    (format t "Results table:~%")
    (dolist (c tbl) (format t "~30a ~a~%" (car c) (cdr c)))
    (format t "~{~#[No winners~;Winner is ~a~;Winners are ~a and ~a~:;Winners are ~@{~#[~; and~] ~a~^,~}~]~}.~%" (mapcar #'car (subseq tbl 0 seats)))))

(add-election-method
 "Borda" #'borda-output
 :description

 "The Borda count is a voting system used for single-winner elections in which
each voter rank-orders the candidates.

The Borda count was devised by Jean-Charles de Borda in June of 1770. It was
first published in 1781 as “Mémoire sur les élections au scrutin” in the
Histoire de l'Académie Royale des Sciences, Paris. This method was devised by
Borda to fairly elect members to the French Academy of Sciences and was used by
the Academy beginning in 1784 until quashed by Napoleon in 1800.

The Borda count is classified as a positional voting system because each rank on
the ballot is worth a certain number of points.")
