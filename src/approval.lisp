;;;; Approval vote method
(in-package :info.fkhodkov.rankedchoice.approval-vote)

(defun approval-vote (ballots candidates seats translation)
  (let* ((res (positional (lambda (i) 1) ballots (length translation)))
           (tbl (map 'list (lambda (c) (cons (nth (aref translation (car c)) candidates)
                                             (cdr c)))
                     res)))
      (format t "Results table:~%")
      (dolist (c tbl) (format t "~30a ~a~%" (car c) (cdr c)))
      (format t "~{~#[No winners~;Winner is ~a~;Winners are ~a and ~a~:;Winners are ~@{~#[~; and~] ~a~^,~}~]~}.~%" (mapcar #'car (subseq tbl 0 seats)))))

(add-election-method "Approval" #'approval-vote)
