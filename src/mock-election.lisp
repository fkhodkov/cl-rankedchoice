;;;; Generate a primitive mock election for testing purposes
(in-package :info.fkhodkov.rankedchoice.mock-election)

(deftype coordinate ()
  `(single-float -10.0 10.0))

(defun manhattan-distance (x1 y1 x2 y2)
  (declare (optimize (speed 3)) (coordinate x1 y1 x2 y2))
  (+ (abs (- x1 x2)) (abs (- y1 y2))))

(defun random-fill-arrays (&rest arrays)
  (declare (optimize (speed 3)))
  (dolist (array arrays)
    (declare (type (simple-array coordinate) array))
    (iter (for i index-of-vector array)
          (setf (aref array i) (+ -10.0 (random 20.0))))))

(defun distances-array (x0 y0 xs ys)
  (declare (optimize (speed 3)) (coordinate x0 y0) (type (simple-array coordinate) xs ys))
  (map 'vector (lambda (x y) (manhattan-distance x0 y0 x y)) xs ys))

(defun mock-election (candidates voters seats &optional (stream *standard-output*))
  (declare (optimize (speed 3)) (fixnum candidates voters seats))
  (let ((cand-x (make-array candidates :element-type 'coordinate))
        (cand-y (make-array candidates :element-type 'coordinate))
        (vote-x (make-array voters :element-type 'coordinate))
        (vote-y (make-array voters :element-type 'coordinate)))
    (random-fill-arrays cand-x cand-y vote-x vote-y)
    (let ((current-voter 0))
      (declare (small-natural current-voter))
      (flet ((generate-ballot ()
               (when (< current-voter voters)
                 (let* ((dists (distances-array
                                (aref vote-x current-voter) (aref vote-y current-voter)
                                cand-y cand-y))
                        (ballot (make-array candidates
                                            :element-type 'list :initial-element nil))
                        (order (sort (make-range candidates) #'<
                                     :key (lambda (c) (aref dists c))))
                        (cutoff (random candidates)))
                   (iter (for candidate in-vector order)
                         (for i to cutoff) (declare (fixnum i))
                         (push candidate (aref ballot i))
                         (yield (incf current-voter)
                                (make-instance 'ballot
                                               :order (subseq ballot 0 cutoff)
                                               :weight 1)))))))
        (output-blt
         #'generate-ballot
         (mapcar
          (lambda (c)
            (declare (small-natural c))
            (format nil "C~d (~f ~f)" (1+ c) (aref cand-x c) (aref cand-y c)))
          (make-range candidates :type :list))
         (format
          nil "Mock election: ~d candidates, ~d seats, ~d voters"
          candidates seats voters)
         seats :stream stream)))))
