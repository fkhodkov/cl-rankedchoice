# Rankedchoice #

Rankedchoice is a software to calculate results of various
[ranked-choice, or preferential](https://en.wikipedia.org/wiki/Ranked_voting)
voting systems.

For now, it supports following methods:

1. [Single Transferable Vote](https://en.wikipedia.org/wiki/Single_transferable_vote)
   variants:
   1. Proportional representation foundation's
   [Reference Weighted Inclusive Gregory Method Rule](http://prfound.org/resources/reference/reference-wigm-rule/).
   2. [Scottish Local Government Elections Order 2007](http://www.legislation.gov.uk/ssi/2007/42/pdfs/ssi_20070042_en.pdf).

2. [Schulze method](https://en.wikipedia.org/wiki/Schulze_method)
3. [Borda count](https://en.wikipedia.org/wiki/Borda_count)
4. [Approval vote](https://en.wikipedia.org/wiki/Approval_vote)
5. [QPQ, a quota-preferential STV-like election rule](http://www.votingmatters.org.uk/ISSUE17/I17P1.PDF)

It takes input in the form of .blt file containing information about
election, candidates and ballots and prints election results on
standard output.
